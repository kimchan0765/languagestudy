const mysql = require('mysql');

const connection = mysql.createConnection({
    host     : '115.71.233.22',
    user     : 'testuser',
    password : 'testuser!@#',
    database : 'testdb'
});

//connect할때 process하나가 생기고 그 process가 connection이 됬을때 
//이후의 query문을 수행한다.
connection.connect();

connection.beginTransaction(err2 => {
    connection.query('update User set lastLogin=now() where uid=?', ['user2'], function (error, results, fields) {
        if (error) throw error;
        console.log('The Second User is: ', results[0]);
    
        connection.query('select * from User where udi=?', ['user2'], function (error, results, fields) {
            if (error) throw error;
            console.log('The First User is: ', results[0]);
    
            connection.query('delete', (error) => {
                if(error) throw error;
                connection.end();
            });      
        });
    });     
});