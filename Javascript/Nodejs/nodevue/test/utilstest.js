const util = require('util');

const utils = require('../utils');

let map = utils.makeMap('name', 'hong');
util.log("map>>>>>>>>>>", map);

return;
let str = "NodeJS";

let enc = utils.encrypt(str);
util.log("enc = ", enc);                            //U2FsdGVkX19TYTqwjw+wOp+JfjYXcUmrvQZdPtnPE60=
// util.log("enc = ", utils.encrypt(str, 'aaa'));   //U2FsdGVkX19Wk5x1Lr5du8zUZbqweQ7uDl26Au+9BMk=
let dec = utils.decrypt(enc);
util.log("dec = ", dec);    //4e6f64654a53

let shaEnc = utils.encryptSha2(str);
util.log("shaEnc = ", shaEnc);                      //a9090cacef4a9dd0e0d452f889518c0660c6fc8e810d9111253dd4dbd14261a7

return;
let url = "https://naver.com";

utils.ogsinfo(url, (err, ret) => {
    util.log(err, ret);
});