const express = require('express'), //웹서버 = express
      app = express(),
      util = require('util');

const Pool = require('./pool'),
      Mydb = require('./mydb');

const testJson = require('./test/test.json');
      
const pool = new Pool();

                    //html, css, 같은 것들은 public 밑에 존재, 정적인 자원들
app.use(express.static('public'));
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

//:email에서 :은 uri에서 변수값을 가져온다는 것.
app.get('/test/:email', (req, res) => {
    testJson.email = req.params.email;  // cf. req.body, req.query
    testJson.aa    = req.query.aa;    
    res.json(testJson);
}); 

app.get('/dbtest/:user', (req, res) => {
    let user = req.params.user;
    let mydb = new Mydb(pool);
    mydb.execute( conn => {
        conn.query("select * from User where uid = ?", [user], (err, ret) => {
            res.json(ret);
        });
    });
}); 

const server = app.listen(7000, function(){
    console.log("Express's started on port 7000");
});

//socket.io는 실시간 채팅을 위한 모듈, 가장큰 장점은 거의 모든 브라우저에 지원해준다.
//socket object
//  -- socket이 기본적으로 가지고 있는 것 => id, rooms(client socket이 어느 방에 들어가 있는지 알려주는 정보)
//  -- handshake : socket.handshake, client와 server간에 존재. query, host, url, user-agent, cookie... 

//roooms?
//  -- socket.broadcast.emit(...)
//  -- socket.broadcast.to('roomId').emit(...)
//  -- io.to('roomId').emit(...)

//                                    웹서버에 socket을 얹히는 것. (단독으로 열수도 있지만 다른 것도 제공하는 경우가 많기 때문에...)
const io = require('socket.io').listen(server, {
    log: false,
    origins: '*:*',
    pingInterval: 3000,
    pingTimeout: 5000
});
  
//client의 모든 sockets(소켓 들!)에 대해서 연결이 된 상태면! 안에 것을 실행.
io.sockets.on('connection', (socket, opt) => {
    //on은 socket을 통해 data를 받는것, emit는 socket을 통해 data를 보내는것.
    socket.emit('message', {msg: 'Welcome ' + socket.id});
  

    util.log("connection>>", socket.id, socket.handshake.query)
  
    //join 기능           (data from client, callback function)
    socket.on('join', function(roomId, fn) {
        //join이 되고나면 나에게 리턴되는건 함수 function()
        socket.join(roomId, function() {
                                    //socket.rooms(방목록)은 json array. json의 key값만 뽑아올때는 Object.json(...) 사용.
                                    //{roomId: [socket1, socket2, ...]}
                                    //이렇게하면 방목록들이 array로 내려옴
            util.log("Join", roomId, Object.keys(socket.rooms));
            if (fn)
                fn();
        });
    });
    
    //leave 기능                
    socket.on('leave', function(roomId, fn) {
        util.log("leave>>", roomId, socket.id)
        socket.leave(roomId, function() {
        if (fn)
            fn();
        });
    });
  
    socket.on('rooms', function(fn) {
        if (fn)
            fn(Object.keys(socket.rooms));
    });
  
    // data: {room: 'roomid', msg: 'msg 내용..'}
    socket.on('message', (data, fn) => {
        //client로부터 data를 받으면 util.log로 찍고 아래의 socket.broadcast.to('roomid')로 데이터를 보낸다. 
        util.log("message>>", data);
  
        socket.broadcast.to(data.room).emit('message', {room: data.room, msg: data.msg});
        //콜백함수가 있으면 아래의 결과를 다시 보냄.
        if (fn)
            fn(data.msg);
    });
                                //socketid(수신자), msg( String, chat-client.html의 "귓속말:" + $('#msg').val() )
    socket.on('message-for-one', (socketid, msg, fn) => {
        socket.broadcast.to(socketid).emit('message', {msg: msg});
    });
  
    //disconnecting은 disconnect하기 전. disconnecting을 쓰면 socket정보를 좀더 쉽게 알아낼 수 있다.
    socket.on('disconnecting', function(data) {
        util.log("disconnecting>>", socket.id, Object.keys(socket.rooms))
    });
  
    socket.on('disconnect', function(data) {
        util.log("disconnect>>", socket.id, Object.keys(socket.rooms))
    }); 
  
  });

