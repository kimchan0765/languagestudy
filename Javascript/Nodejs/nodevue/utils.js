const ogs = require('open-graph-scraper'),
      HashMap = require('hashmap'),
      Crypto = require('crypto-js'),
      SHA256 = ("crypto-js/sha256");

const EKey = "nodevue"; //암호화할때 이 key를 모르면 절대 복호화할 수 없다.

module.exports = {

    makeMap(key, value){
        const map = new HashMap();
        map.set(key, value);

        console.log("TTT>>", map.get(key));
        
        return map;
    },

    encryptSha2(data, key){
        if(!data) return null;
        key = key || EKey;

        try {
            return Crypto.SHA256(data + key).toString();
        } catch(Err) {
            console.log("Error on encryptoSha2::", err);
        }
    },

    encrypt(data, key) {
        //양방향 암호화 = AES, 단방향 암호화 = SHA
        return Crypto.AES.encrypt(data, key || EKey).toString();
    },

    decrypt(data, key) {
        return Crypto.AES.decrypt(data, key || EKey).toString(Crypto.enc.Utf8);
    },

    ogsinfo(url, fn) {
        return ogs({url : url}, (err, ret) => {
            fn(err, ret);
        });
    }

};