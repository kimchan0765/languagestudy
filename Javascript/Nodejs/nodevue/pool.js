const mysql   = require("mysql"),
      util    = require('util'),
      Promise = require("bluebird");

Promise.promisifyAll(mysql);
Promise.promisifyAll(require("mysql/lib/Connection").prototype);
Promise.promisifyAll(require("mysql/lib/Pool").prototype);

const DB_INFO = {
  host     : '115.71.233.22',
  user     : 'testuser',
  password : 'testuser!@#',
  database : 'testdb',
  multipleStatements: true,
  connectionLimit: 5,
  waitForConnections: false
};

module.exports = class {
    constructor(dbinfo) {
        dbinfo = dbinfo || DB_INFO;
        this.pool = mysql.createPool(dbinfo);
    }
    
    connect() {
        //>>>>>pool에서 conncection을 가져오고 꺼내오는 그 process가 종료됬을때, disposer가 실행된다(connection을 닫아야할때 실행됨). 
        return this.pool.getConnectionAsync().disposer(conn => {
            //재사용해야되기 때문에 close()가 아니라 release()
            return conn.release();
        });
    }
  
    end() {
        this.pool.end( function(err) {
            util.log(">>>>>>>>>>>>>>>>>>>>>>>>>>> End of Pool!!");
        if (err)
            util.log("ERR pool ending!!");
        });
    }
};
  
