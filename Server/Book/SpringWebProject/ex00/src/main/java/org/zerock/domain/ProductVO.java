package org.zerock.domain;

import lombok.Data;

@Data
public class ProductVO {
	
	private String name;
	private double price;
	
	public ProductVO(String name, double price) {
		super();
		this.name = name;
		this.price = price;
	}
}
