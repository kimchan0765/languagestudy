package org.zerock.persistence;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;
import org.zerock.domain.MemberVO;

@Repository
public class MemberDAOImpl implements MemberDAO {

	@Inject
	private SqlSession session;
	
	private static final String NS = "org.zerock.mapper.MemberMapper";
	private static final String GETTIME = NS + ".getTime";
	private static final String INSERT_MEMBER = NS + ".insertmember";
	
	
	@Override
	public String getTime() {
		return session.selectOne(GETTIME);
	}

	@Override
	public void insertMember(MemberVO vo) {
		session.insert(INSERT_MEMBER, vo);
	}

}
