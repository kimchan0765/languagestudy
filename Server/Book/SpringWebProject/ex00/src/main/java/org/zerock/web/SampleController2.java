package org.zerock.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SampleController2 {

	private static final Logger logger = LoggerFactory.getLogger(SampleController.class);
	
	@RequestMapping("doC")
	public String doC(@ModelAttribute("msg") String msg) {
		//@ModelAttribute는 자동으로 해당 객체를 뷰까지 전달합니다.
		logger.info("doC called......");
		
		return "result";
	}
}
